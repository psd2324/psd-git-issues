# Part 1

* Clone this repository to your machine.
* Find the file for your group, and edit it to add your preferred name and where in the room you are sitting.
* Commit this change.
* Push this change to the repository.
* Find the other people in your group and sit with them.

# Part 2

* Pull the repository
* Create a new branch
* Edit the file for your group again, and add one skill you have that will help the group in the assessment
* Push the branch
* Use the Gitlab web interface to create a new merge request, and give it to the group member above you in the list to review (wrapping around, e.g. Student B -> Student A, Student C -> Student B, Student A -> Student C)
* Review the merge request that you've been given, making comments and accepting / rejecting as appropriate
* Discuss with your group which ways you think you might prefer to work
* As a group, review the following resources and discuss whether you think they will be appropriate for you:
  * https://www.ed.ac.uk/institute-academic-development/study-hub/learning-resources/group-working
